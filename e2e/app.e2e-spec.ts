import { Ng2DataFlowPlaygroundPage } from './app.po';

describe('ng2-data-flow-playground App', () => {
  let page: Ng2DataFlowPlaygroundPage;

  beforeEach(() => {
    page = new Ng2DataFlowPlaygroundPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
