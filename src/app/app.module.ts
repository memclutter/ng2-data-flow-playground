import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {LeftModule} from './modules/left/left.module';
import {RightModule} from './modules/right/right.module';
import { TopComponent } from './components/top/top.component';
import { BottomComponent } from './components/bottom/bottom.component';

@NgModule({
  declarations: [
    AppComponent,
    TopComponent,
    BottomComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    LeftModule,
    RightModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
