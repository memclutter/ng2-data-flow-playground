import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RightComponent} from './components/right/right.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [RightComponent],
  exports: [RightComponent]
})
export class RightModule { }
